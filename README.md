# Wardrobify

Team:

* Justin Cosby - Hats
* Nahom Zeleke - Shoes

## Design
![alt text](image.png)
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

We were provided with all the necessary data from the wardrobe microservice to build and make a shoe microservice that interacts with the wardrobe microservice to allow the user to see the list of shoes, create a shoe, and delete a show. The BinVO instance in the models allows the link between the two microservices in order to get the data for the different bins and assign in to the the shoe.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Created a Hat model with its parameters and a LocationVO model. The purpose of that is to use api(back-end) to assist react(front-end) to create a web browser. We created a LocationVO so we can use the Location model from Wardrobe to assist us with the api. These models' parameters were then used to create and display hats on the web browser for the user.
