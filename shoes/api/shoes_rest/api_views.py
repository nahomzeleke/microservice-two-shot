from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json


class BinVODetialEncoder(ModelEncoder):
    model=BinVO
    properties = ["import_href", "closet_name"]

class ShoeListEncoder(ModelEncoder):
    model=Shoe
    properties = ["id","manufacturer", "name", "color", "picture_url", "bin"]

    encoders = {
        "bin": BinVODetialEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "name", "color", "picture_url", "bin"]

    encoders = {
        "bin": BinVODetialEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
@require_http_methods(["GET", "DELETE"])
def api_delete_shoes(request, pk):

    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:

        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
