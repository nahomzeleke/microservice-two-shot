import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatsList';
import HatForm from './HatForm';
import HatDetail from './HatDetail';
import ShoesList from './ShoesList'
import ShoeForm from './ShoeForm';
import ShoeDetail from './ShoeDetail';
import Nav from './Nav';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList hats={props.hats} />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/hats/:id" element={<HatDetail hats={props.hats} />} />
          <Route path="shoes">
             <Route index element={<ShoesList shoes={props.shoes}/>} />
             <Route path= "new" element={<ShoeForm />}/>
             <Route path=":id" element={<ShoeDetail shoes={props.shoes}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
