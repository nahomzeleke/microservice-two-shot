import { NavLink } from "react-router-dom";

function HatsList(props) {

    const onDelete = (id) => {
    return async () => {
      await fetch(`http://localhost:8090/api/hats/${id}/`, {method:"delete"})
      alert("delete successful")
      window.location.reload()
    }
    };

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Style Name</th>
          </tr>
        </thead>
        <tbody>
          {props.hats?.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.id }</td>
                <td>
                  <NavLink className="nav-link" to={`/hats/${hat.id}`}>{hat.style_name}</NavLink>
                </td>
                <td>
                  <button onClick={onDelete(hat.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatsList;
