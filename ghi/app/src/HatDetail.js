import React, {useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'

function HatsDetail() {

    const [hat, setHat] = useState('')
    const {id} = useParams()

    const fetchData = async () => {
      const url = `http://localhost:8090/api/hats/${id}`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setHat(data);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

    return(
      <div key={hat.id} className="card mb-3 shadow">
          <img src={hat.picture_url} className="mx-auto p-2" width="500" />
          <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                  {hat.location?.closet_name}
                  {hat.location?.shelf_number}
              </h6>
              <p className="card-text">{hat.fabric}</p>
              <p className="card-text">{hat.color}</p>
          </div>
      </div>
    )
    }

  export default HatsDetail;
