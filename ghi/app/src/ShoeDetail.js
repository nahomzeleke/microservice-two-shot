import React, {useEffect,useState} from 'react'
import { useParams } from 'react-router-dom'
function ShoeDetail() {

    const [shoe, setShoe] = useState('')
    const {id} = useParams()

    const fetchData = async () => {
        const url = `http://localhost:8080/api/shoes/${id}`

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setShoe(data)

        }
    }

    useEffect (() => {
        fetchData()

    }, [])

    const onDelete = (id) => {
        return async () => {
            await fetch(`http://localhost:8080/api/shoes/${id}/`, {method:"delete"})
            alert("Deleted!!")
            window.location.reload()

        }
          }



    return(
    <div key={shoe.id} className="card mb-3 shadow">
        <img src={shoe.picture_url} className="mx-auto p-2" width="500"  />
        <div className="card-body">
            <h5 className="card-title">{shoe.name}</h5>
            <p className="card-text">Manufacturer - {shoe.manufacturer}</p>
            <p className="card-text">Bin - {shoe.bin?.closet_name}</p>
            <p className="card-text">Color - {shoe.color}</p>
        </div>
    </div>
    )
}
export default ShoeDetail
