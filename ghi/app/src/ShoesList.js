import { NavLink } from "react-router-dom"

function ShoeList(props) {

    const onDelete = (id) => {
    return async () => {
        await fetch(`http://localhost:8080/api/shoes/${id}/`, {method:"delete"})
        alert("Deleted!!")
        window.location.reload()

    }
      }

    return (

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
              {props.shoes?.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>
                    <NavLink className="nav-link" to={`/shoes/${shoe.id}`}>{shoe.name}</NavLink>
                  </td>
                  <td>{ shoe.manufacturer }</td>
                  <td>
                    <button onClick={onDelete(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}

export default ShoeList;
