function createCard(closet_name, section_number, shelf_number, pictureUrl) {
    return `
        <div class="col shadow-lg p-3 mb-5 bg-body-tertiary rounded mx-1">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${closet_name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${section_number}</h6>
                <p class="card-text">${shelf_number}</p>
                </div>
            </div>
        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:3000/hats/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let hat of data.hats) {
          const detailUrl = `http://localhost:3000${hat.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.hat.closet_name;
            const section = details.hat.section_number;
            const shelf = details.hat.shelf_number;
            const html = createCard(title, section, shelf);
            const row = document.querySelector('.row');
            row.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
