# Generated by Django 4.0.3 on 2024-03-13 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='name',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
